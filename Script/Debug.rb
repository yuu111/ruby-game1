#! ruby

module Debug
  def log
    if $DEBUG_FLAG
      puts "DebugLog"
      puts "縦:#{Setting.instance.width}"
      puts "横:#{Setting.instance.height}"
    end
  end
  module_function :log
end
